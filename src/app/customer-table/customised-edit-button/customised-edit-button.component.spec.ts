import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomisedEditButtonComponent } from './customised-edit-button.component';

describe('CustomisedEditButtonComponent', () => {
  let component: CustomisedEditButtonComponent;
  let fixture: ComponentFixture<CustomisedEditButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomisedEditButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomisedEditButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
