import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUserDetailsComponent } from './product-user-details.component';

describe('ProductUserDetailsComponent', () => {
  let component: ProductUserDetailsComponent;
  let fixture: ComponentFixture<ProductUserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUserDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
