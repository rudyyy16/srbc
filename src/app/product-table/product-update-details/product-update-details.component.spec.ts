import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUpdateDetailsComponent } from './product-update-details.component';

describe('ProductUpdateDetailsComponent', () => {
  let component: ProductUpdateDetailsComponent;
  let fixture: ComponentFixture<ProductUpdateDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUpdateDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUpdateDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
