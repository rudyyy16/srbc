import { Component, OnInit, Inject } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

 

@Component({

  selector: 'app-product-update-details',

  templateUrl: './product-update-details.component.html',

  styleUrls: ['./product-update-details.component.css']

})

export class ProductUpdateDetailsComponent implements OnInit {

 

  constructor(public dialogRef: MatDialogRef<ProductUpdateDetailsComponent>, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) {

    console.log(this.data);

  }

  onNoClick(): void {

    this.dialogRef.close();

  }

 

 

  productDetailForm = this.fb.group({

    product_id: [this.data.product_id],

    product_name: [this.data.product_name],

    product_price: [this.data.product_price],

    product_quantity: [this.data.product_quantity],

    product_pincode: [this.data.product_pincode],

 

  })

 

  ngOnInit() {

  }

 

}

 