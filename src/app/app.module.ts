import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatModule } from './mat/mat.module';
// import { CustomerDetailsModule } from './customer-details/customer-details.module';
import { CustomerTableModule } from './customer-table/customer-table.module';
import { ProductTableModule } from './product-table/product-table.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatModule,
    CustomerTableModule,
    ProductTableModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [SidenavComponent],
  bootstrap: [AppComponent]
})

export class AppModule { }