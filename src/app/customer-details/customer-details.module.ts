import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CustomerDetailsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports :[CustomerDetailsComponent],
})
export class CustomerDetailsModule { }
